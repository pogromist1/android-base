package pro.android.base.permissions

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.MultiplePermissionsState
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import pro.android.base.R

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun PermissionRequester(
    requiredPermissions: Collection<String>,
    onGranted: @Composable BoxScope.() -> Unit,
) {
    val permissionState =
        rememberMultiplePermissionsState(permissions = requiredPermissions.toList())

    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        if (permissionState.allPermissionsGranted)
            return onGranted()

        if (permissionState.shouldShowRationale) {
            RequestPermission(permissionState)
        } else {
            SideEffect { permissionState.launchMultiplePermissionRequest() }
        }
    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
private fun RequestPermission(permissionState: MultiplePermissionsState) {
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .animateContentSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    )
    {
        Text(
            text = context.getString(R.string.permission_requester_need_to_grant),
            textAlign = TextAlign.Center
        )
        Button(onClick = { permissionState.launchMultiplePermissionRequest() }) {
            Text(text = context.getString(R.string.permission_requester_grant_permission))
        }
    }
}