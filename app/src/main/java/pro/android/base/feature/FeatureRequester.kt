package pro.android.base.feature

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import pro.android.base.R

@Composable
fun FeatureRequester(
    requiredFeature: FeatureDefinition,
    onFeatureEnabled: @Composable BoxScope.() -> Unit
) {
    val context = LocalContext.current;
    val hasFeature = context.packageManager.hasSystemFeature(requiredFeature.packageFeatureName)

    var isEnabled by remember {
        mutableStateOf(requiredFeature.stateChecker())
    }
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        when {
            !hasFeature -> Text(
                context.getString(R.string.feature_requester_no_feature_available) + requiredFeature.packageFeatureName,
                textAlign = TextAlign.Center
            )

            hasFeature && isEnabled -> onFeatureEnabled()

            else -> EnableFeature(context, requiredFeature) { isEnabled = true }
        }
    }
}

@Composable
fun EnableFeature(context: Context, requiredFeature: FeatureDefinition, onEnabled: () -> Unit) {
    val enableFeatureRequest =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) onEnabled()
        }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp),
    ) {
        Text(text = requiredFeature.requestText)
        Button(
            onClick = {
                enableFeatureRequest.launch(Intent(requiredFeature.intentAction))
            },
        ) {
            Text(text = context.getString(R.string.feature_requester_turn_on))
        }
    }
}

data class FeatureDefinition(
    val packageFeatureName: String,
    val intentAction: String,
    val requestText: String,
    val stateChecker: () -> Boolean
)