package pro.android.base

import android.annotation.SuppressLint
import android.bluetooth.BluetoothManager
import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.style.TextAlign
import androidx.core.content.getSystemService
import pro.android.base.bluetooth.BluetoothObserver
import pro.android.base.bluetooth.BluetoothPermissionRequester
import pro.android.base.bluetooth.BluetoothRequester
import pro.android.base.ui.theme.BaseDemoTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val context = LocalContext.current
            BaseDemoTheme {
                Surface(modifier = Modifier.fillMaxSize()) {
                    BluetoothPermissionRequester() {
                        MainScreen(context)
                    }
                }
            }
        }
    }

    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    @Composable
    private fun MainScreen(context: Context) {
        val bluetoothAdapter = context.getSystemService<BluetoothManager>()?.adapter
        var bluetoothEnabled by remember { mutableStateOf(bluetoothAdapter?.isEnabled == true) }
        val bluetoothObserver = BluetoothObserver(LocalContext.current) { enabled ->
            bluetoothEnabled = enabled
        }

        if (bluetoothEnabled) {
            ShowScaffold(context)
        } else {
            BluetoothRequester(context) {
                bluetoothEnabled = true
            }
        }

        LaunchedEffect(true) {
            bluetoothObserver.startObserve()
        }

        DisposableEffect(LocalLifecycleOwner.current) {
            onDispose {
                bluetoothObserver.stopObserve()
            }
        }
    }

    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    @Composable
    private fun ShowScaffold(context: Context) {
        Scaffold(
        ) {
            Box(Modifier.fillMaxSize()) {
                Text(
                    context.getString(R.string.all_permissions_granted),
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}