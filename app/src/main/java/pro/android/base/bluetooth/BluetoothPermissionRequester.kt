package pro.android.base.bluetooth

import android.Manifest
import android.os.Build
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.runtime.Composable
import pro.android.base.permissions.PermissionRequester

@Composable
fun BluetoothPermissionRequester(onGranted: @Composable BoxScope.() -> Unit) {
    // If we derive physical location from BT devices or if the device runs on Android 11 or below
    // we need location permissions otherwise we don't need to request them (see AndroidManifest).

    val locationPermissionSet = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
        setOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
        )
    } else {
        setOf(
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    // For Android 12 and above we only need connect and scan
    val bluetoothPermissionSet = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        setOf(
            Manifest.permission.BLUETOOTH_CONNECT,
            Manifest.permission.BLUETOOTH_SCAN,
        )
    } else {
        setOf(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
        )
    }

    PermissionRequester(locationPermissionSet + bluetoothPermissionSet ) {
        onGranted()
    }
}