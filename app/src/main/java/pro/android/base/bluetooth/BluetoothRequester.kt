package pro.android.base.bluetooth

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.pm.PackageManager
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.runtime.Composable
import androidx.core.content.getSystemService
import pro.android.base.R
import pro.android.base.feature.FeatureDefinition
import pro.android.base.feature.FeatureRequester

@Composable
fun BluetoothRequester(context: Context, onBluetoothEnabled: @Composable BoxScope.() -> Unit) {
    val requiredFeature = FeatureDefinition(
        PackageManager.FEATURE_BLUETOOTH,
        BluetoothAdapter.ACTION_REQUEST_ENABLE,
        context.getString(R.string.bluetooth_is_off)
    ) { context.getSystemService<BluetoothManager>()?.adapter?.isEnabled == true }

    FeatureRequester(requiredFeature) {
        onBluetoothEnabled()
    }
}